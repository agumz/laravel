<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>New form</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
   
    <form action="/welcome" method="POST">
        <label for="nama_user">First Name :</label>
        <input type = "text" id="nama_user">
        <br><br>
        <label for="last_name">Last Name :</label>
        <input type = "text" id="last_name">
        <br> <br>

        <label>Gender</label> <br><br>
        <input type="radio" name="gender" value="0" checked>Laki-laki<br>
        <input type="radio" name="gender" value="1" >Perempuan <br>
        <input type="radio" name="gender" value="2" >Other 
        <br><br>

        <label>Nationality</label> <br><br>
        <select>
            <option value="0">Indonesia</option>
            <option value="1">Malaysia</option>
            <option value="2">Japan</option>
            <option value="3">USA</option>
            <option value="4">UK</option>
            <option value="5">China</option>
        </select> 

        <br><br>
        <label>Leanguage Spoken</label> <br><br>
        <input type="radio" name="leanguage Spoken" value="0" checked> Bahasa Indonesia <br>
        <input type="radio" name="leanguage Spoken" value="1" checked> English <br>
        <input type="radio" name="leanguage Spoken" value="2" checked> Other <br>
        <br><br>

        <label for="bio">Bio</label> <br> <br>
        <textarea cols="30" rows="7" id="bio"></textarea>
        <br>
        <input type="submit" value="Sign Up">   
    </form>
</body>
</html>